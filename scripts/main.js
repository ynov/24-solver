window.game.getTimeLimit = function() { return 9999; }

window.B = window.game.getBubbles;
window.N = window.game.getNumbers;
window.M = [];

window.Play = function() {
    $('#play').click();
    window.M = N();
};

window.Restart = function() {
    $('#restart').click();
    window.M = N();
}

window.Next = function() {
    $('#next-level').click();
    window.M = N();
}

window.Unsolvable = function() {
    $('#no-solution').click();
    window.M = N();
}

window.LastAns = -1;
window.Ans = [];
window.ILastAnst = function() {
    if (window.LastAns < 0)
        return;
    return window.Ans[window.LastAns];
};
window.IOf = function(i) {
    var index = M.indexOf(parseInt(i));
    M[index] = null;
    return index;
};

window.E = function(i) { return $('#bubble-' + i); }
window.ES = function() {
    var es = [];
    for (var i = 0; i < 4 + window.LastAns; i++) {
        es.push(E(i));
    }
    return es;
};

window.Join = function(i, j) {
    window.DropEl(E(i), E(j));
};

window.Disjoin = function(i) {
    E(i).click();
};

window.Add = function() {
    $('#add').click();
};

window.Sub = function() {
    $('#subtract').click();
};

window.Mul = function() {
    $('#multiply').click();
};

window.Div = function() {
    $('#divide').click();
};

window.ASMD = function(op) {
    if (op === '+') {
        Add();
    } else if (op === '-') {
        Sub();
    } else if (op === 'x') {
        Mul();
    } else if (op === '/') {
        Div();
    }
};

// (9 - 5) x (7 - 1)
window.RgxA = /\((\d+) ([+-x/]) (\d+)\) ([+-x/]) \((\d+) ([+-x/]) (\d+)\)/;
// ((10 - 7) x 5) + 9
window.RgxB = /\(\((\d+) ([+-x/]) (\d+)\) ([+-x/]) (\d+)\) ([+-x/]) (\d+)/;
// 10 x (2 + (4 / 10))
window.RgxC = /(\d+) ([+-x/]) \((\d+) ([+-x/]) \((\d+) ([+-x/]) (\d+)\)\)/;

window.Auto = function(str) {
    var match;
    var TTT = 300;
    var TTTM = 800;

    console.log(str);
    if (str === 'Not Found') {
        Unsolvable();
        setTimeout(function() {
            Next();
            AutoSolve();
        }, TTTM);
        return;
    }

    ///////////////////////////////////////////////////////////////////////////
    match = str.match(RgxA);
    if (match) {
        match.splice(0, 1);
        console.log(match);

        var NN1 = match[0];
        var NN2 = match[2];
        var OP1 = match[1];
        var NN3 = match[4];
        var NN4 = match[6];
        var OP2 = match[5];
        var OP3 = match[3];

        setTimeout(function() {
            Join(IOf(NN1), IOf(NN2));
            setTimeout(function() {
                ASMD(OP1);
                setTimeout(function() {
                    Join(IOf(NN3), IOf(NN4));
                    setTimeout(function() {
                        ASMD(OP2);
                        setTimeout(function() {
                            Join(Ans[0], Ans[1]);
                            setTimeout(function() {
                                ASMD(OP3);
                                setTimeout(function() {
                                    Next();
                                    AutoSolve();
                                }, TTTM);
                            }, TTT);
                        }, TTT);
                    }, TTT)
                }, TTT);
            }, TTT);
        }, TTT);

        return;
    }

    ///////////////////////////////////////////////////////////////////////////
    match = str.match(RgxB);
    if (match) {
        match.splice(0, 1);
        console.log(match);

        var NN1 = match[0];
        var NN2 = match[2];
        var OP1 = match[1];
        var NN3 = match[4];
        var OP2 = match[3];
        var NN4 = match[6];
        var OP3 = match[5];

        setTimeout(function() {
            Join(IOf(NN1), IOf(NN2));
            setTimeout(function() {
                ASMD(OP1);
                setTimeout(function() {
                    Join(Ans[0], IOf(NN3));
                    setTimeout(function() {
                        ASMD(OP2);
                        setTimeout(function() {
                            Join(Ans[1], IOf(NN4));
                            setTimeout(function() {
                                ASMD(OP3);
                                setTimeout(function() {
                                    Next();
                                    AutoSolve();
                                }, TTTM);
                            }, TTT);
                        }, TTT);
                    }, TTT)
                }, TTT);
            }, TTT);
        }, TTT);

        return;
    }

    ///////////////////////////////////////////////////////////////////////////
    match = str.match(RgxC);
    if (match) {
        match.splice(0, 1);
        console.log(match);

        var NN1 = match[0];
        var NN2 = match[2];
        var OP1 = match[1];
        var NN3 = match[4];
        var OP2 = match[3];
        var NN4 = match[6];
        var OP3 = match[5];

        setTimeout(function() {
            Join(IOf(NN3), IOf(NN4));
            setTimeout(function() {
                ASMD(OP3);
                setTimeout(function() {
                    Join(IOf(NN2), Ans[0]);
                    setTimeout(function() {
                        ASMD(OP2);
                        setTimeout(function() {
                            Join(IOf(NN1), Ans[1]);
                            setTimeout(function() {
                                ASMD(OP1);
                                setTimeout(function() {
                                    Next();
                                    AutoSolve();
                                }, TTTM);
                            }, TTT);
                        }, TTT);
                    }, TTT)
                }, TTT);
            }, TTT);
        }, TTT);

        return;
    }
};

window.AutoSolve = function() {
    var server = 'http://127.0.0.1:5000/';
    $.getJSON(server + '?x=' + JSON.stringify(N()) + '&jsoncallback=?', {})
};

Play();
AutoSolve();
