from flask import Flask, request, Response
import json

import operator as op
import itertools as it

def s(x):
    m = {"add": "+", "sub": "-", "mul": "x", "truediv": "/"}
    return m[x.__name__]

def t(x):
    for i in it.permutations(x):
        for j in it.product([op.add, op.sub, op.mul, op.truediv], repeat=3):
            y = j[0](j[1](j[2](i[0], i[1]), i[2]), i[3])
            if y == 24:
                return "((%d %s %d) %s %d) %s %d" % (i[0], s(j[2]),
                    i[1], s(j[1]), i[2], s(j[0]), i[3])
            if not (j[2](i[2], i[3]) == 0 and s(j[0]) == "/"):
                y = j[0](j[1](i[0], i[1]), j[2](i[2], i[3]))
                if y == 24:
                    return "(%d %s %d) %s (%d %s %d)" % (i[0], s(j[1]),
                        i[1], s(j[0]), i[2], s(j[2]), i[3])
            if not ((j[2](i[2], i[3]) == 0 and s(j[1]) == "/") or
            (j[1](i[1], j[2](i[2], i[3])) == 0 and s(j[0]) == "/")):
                y = j[0](i[0], j[1](i[1], j[2](i[2], i[3])))
                if y == 24:
                    return "%d %s (%d %s (%d %s %d))" % (i[0], s(j[0]),
                        i[1], s(j[1]), i[2], s(j[2]), i[3])
    return "Not Found"

app = Flask(__name__)

@app.route('/')
def index():
    res_txt = '0'

    x = json.loads(request.args.get('x', '[]'))
    if len(x) == 4:
        res_txt = "Auto('%s');" % t(x)

    return Response(res_txt, mimetype='text/plain')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
