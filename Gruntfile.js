module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            dist: {
                src: ['scripts/game.js', 'scripts/load.js', 'scripts/main.js'],
                dest: 'out.js'
            }
        },

        uglify: {
            outjs: {
                files: {
                    'out.min.js': ['out.js']
                }
            }
        },

        watch: {
            files: ['scripts/*.js'],
            tasks: ['concat', 'uglify'],
            options: { spawn: false }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'uglify']);
}
